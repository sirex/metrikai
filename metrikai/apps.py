from django.apps import AppConfig


class MetrikaiConfig(AppConfig):
    name = 'metrikai'
