import uuid

from django.db import models
from django.contrib.postgres.fields import JSONField


class Collection(models.Model):
    url = models.URLField()
    title = models.CharField(max_length=256)


class Image(models.Model):
    url = models.URLField()


class User(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)


class ImageRegion(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    image = models.ForeignKey(Image, on_delete=models.SET_NULL, null=True)
    top = models.PositiveSmallIntegerField()
    left = models.PositiveSmallIntegerField()
    width = models.PositiveSmallIntegerField()
    height = models.PositiveSmallIntegerField()


class Data(models.Model):
    image = models.ForeignKey(Image, on_delete=models.SET_NULL, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    data = JSONField()
    approved = models.BooleanField(default=False)


class Approvals(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    data = models.ForeignKey(Data, on_delete=models.CASCADE)
    approved = models.BooleanField(default=False)


class Searches(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    timestamp = models.DateTimeField(auto_now_add=True)
