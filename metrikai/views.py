from django.http import Http404
from django.shortcuts import render

from metrikai.models import Approvals
from metrikai.models import Data
from metrikai.models import Image
from metrikai.models import User
from metrikai.forms import DataForm


# How many approvals needed in order to consider an entry as approved.
APPROVALS_THRESHOLD = 3


def index(request):
    return render(request, 'index.html')


def image(request):
    image = Image.objects.order_by('?').first()
    data = Data.objects.filter(image=image)
    return render(request, 'metrikai/image.html', {
        'iamge': image,
        'data': data,
    })


def submit(request):
    if request.method != 'POST':
        raise Http404("Only POST method allowed.")

    form = DataForm(request.POST)
    if form.is_valid():
        data = form.cleaned_data
        user = User.objects.get(pk=data['user'])
        if data['id']:
            obj = Data.objects.get(pk=data['id'])
            if obj.data == data['data']:
                Approvals.objects.create(
                    user=user,
                    data=obj,
                )
            approvals = (
                Approvals.objects.filter(data=obj, approved=True).count()
            )
            approved = approvals > APPROVALS_THRESHOLD
        else:
            obj = Data()
            approved = False

        obj.image = data['image']
        obj.user = user
        obj.data = data['data']
        obj.approved = approved
        obj.save()

    return render(request, 'name.html', {'form': form})


def search(request):
    Data.objects.filter(approved=True)
    return render(request, 'metrikai/search.html', {
        'data': p
    })
