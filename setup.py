from setuptools import setup, find_packages


setup(
    name='metrikai',
    version='0.1',
    packages=find_packages(),
    scripts=['manage.py'],
    license='AGPL',
    keywords='geneology, family history, handwriting, crowdsourcing',
    url='https://gitlab.com/sirex/metrikai',
    project_urls={
        'Bug Tracker': 'https://gitlab.com/sirex/metrikai/issues',
        'Source Code': 'https://gitlab.com/sirex/metrikai',
    }
)
