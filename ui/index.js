const $ = require('jquery');

window.jQuery = $;

require('bootstrap');
require('bootstrap/dist/css/bootstrap.css');
require('bootstrap/dist/css/bootstrap-theme.css');
require('handlebars');
require('moment');
require('eonasdan-bootstrap-datetimepicker');
require('eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css');
require('alpaca');
require('alpaca/dist/alpaca/bootstrap/alpaca.css');

const interact = require('interactjs');

require('./style.css');


function load_image_tools() {
  var container = document.querySelector('.resize-container');
  $('.resize-drag').css({
    'position': 'absolute',
    'left': container.offsetLeft,
    'top': container.offsetTop,
  });

  interact('.resize-selected')
    .draggable({
      restrict: {
        restriction: 'parent',
        elementRect: { top: 0, left: 0, bottom: 1, right: 1 }
      },
    })
    .resizable({
      edges: { left: true, right: true, bottom: true, top: true },
      restrictEdges: {
        outer: 'parent',
        endOnly: true,
      },
      restrictSize: {
        min: { width: 100, height: 50 },
      },
      inertia: true,
    })
    .on('resizemove', function (event) {
      var target = event.target,
        x = (parseFloat(target.getAttribute('data-x')) || 0),
        y = (parseFloat(target.getAttribute('data-y')) || 0);
      target.style.width  = event.rect.width + 'px';
      target.style.height = event.rect.height + 'px';
      x += event.deltaRect.left;
      y += event.deltaRect.top;
      target.style.webkitTransform = target.style.transform = 'translate(' + x + 'px,' + y + 'px)';
      target.setAttribute('data-x', x);
      target.setAttribute('data-y', y);
    })
    .on('dragmove', function (event) {
      var target = event.target,
        x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx,
        y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;
      target.style.webkitTransform = target.style.transform = 'translate(' + x + 'px, ' + y + 'px)';
      target.setAttribute('data-x', x);
      target.setAttribute('data-y', y);
    });

  var imHeight, imWidth, im = new Image();
  im.onload = function() {
    $('.resize-bg').css({
      width: im.width + 'px',
      height: im.height + 'px'
    });

    interact('.resize-bg')
      .draggable({
        restrictEdges: {
          restriction: 'parent'
        },
      })
      .on('dragmove', function (event) {
        var target = event.target,
          x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx,
          y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;
        target.style.webkitTransform = target.style.transform = 'translate(' + x + 'px, ' + y + 'px)';
        target.setAttribute('data-x', x);
        target.setAttribute('data-y', y);
      });
  }
  im.src = 'http://www.epaveldas.lt/recordImageSmall/ARCH/1356/1/15?exId=289561&seqNr=3';

  $('.resize-bg').on('click', function (event) {
  });

  $('.resize-bg').on('dblclick', function (event) {
    var target = event.target,
      x = (parseFloat(target.getAttribute('data-x')) || 0) + event.x,
      y = (parseFloat(target.getAttribute('data-y')) || 0) + event.y;
    var boxRect = target.getBoundingClientRect();

    var newBox = $('<div class="resize-drag resize-selected">&nbsp;</div>');
    $('.resize-drag').removeClass('resize-selected');
    $(target).append(newBox);
    var a = newBox.data({
      x: event.clientX,
      y: event.clientY,
    });
    newBox.css({
      left: (event.clientX - boxRect.x) + 'px',
      top: (event.clientY - boxRect.y) + 'px',
    });

    newBox.on('click', function (event) {
      $('.resize-drag').removeClass('resize-selected');
      $(event.target).addClass('resize-selected');
    });

  });
}

function load_data_form() {
  $("#form").alpaca({
    "schema": {
      "type": "object",
      "properties": {
        "type": {
          "type": "string",
          "title": "Įrašo tipas",
          "enum": ["gimimo", "santuokos", "mirties"]
        },
        "gimimo": {
          "type": "object",
          "properties": {
            "data": {
              "type": "string",
              "title": "Gimimo data",
              "format": "date"
            },
            "vardas": {
              "type": "string",
              "title": "Vardas"
            },
            "pavarde": {
              "type": "string",
              "title": "Pavardė"
            }
          }
        },
        "santuokos": {
          "type": "object",
          "properties": {
            "data": {
              "type": "string",
              "title": "Santuokos data",
              "format": "date"
            },
            "jaunasis": {
              "type": "object",
              "title": "Jaunasis",
              "properties": {
                "vardas": {
                  "type": "string",
                  "title": "Vardas"
                },
                "pavarde": {
                  "type": "string",
                  "title": "Pavardė"
                },
                "tevas": {
                  "type": "object",
                  "title": "Jaunojo tėvas",
                  "properties": {
                    "vardas": {
                      "type": "string",
                      "title": "Vardas"
                    },
                    "pavarde": {
                      "type": "string",
                      "title": "Pavardė"
                    },
                  }
                },
                "motina": {
                  "type": "object",
                  "title": "Jaunojo motina",
                  "properties": {
                    "vardas": {
                      "type": "string",
                      "title": "Vardas"
                    },
                    "pavarde": {
                      "type": "string",
                      "title": "Pavardė"
                    },
                  }
                }
              }
            },
            "jaunoji": {
              "type": "object",
              "title": "Jaunoji",
              "properties": {
                "vardas": {
                  "type": "string",
                  "title": "Vardas"
                },
                "pavarde": {
                  "type": "string",
                  "title": "Pavardė"
                }
              }
            }
          }
        },
        "mirties": {
          "type": "object",
          "properties": {
            "data": {
              "type": "string",
              "title": "Mirties data",
              "format": "date"
            },
            "vardas": {
              "type": "string",
              "title": "Vardas"
            },
            "pavarde": {
              "type": "string",
              "title": "Pavardė"
            }
          }
        },
      },
      "dependencies": {
        "gimimo": "type",
        "santuokos": "type",
        "mirties": "type"
      }
    },
    "options": {
      "fields": {
        "type": {
          "type": "select"
        },
        "gimimo": {
          "fields": {
            "data": {
              "type": "date",
              "dateFormat": "YYYY-MM-DD",
              // use defaultDate to set a date from the archive book
            }
          },
          "dependencies": {
            "type": "gimimo"
          }
        },
        "santuokos": {
          "fields": {
            "data": {
              "type": "date",
              "dateFormat": "YYYY-MM-DD",
            }
          },
          "dependencies": {
            "type": "santuokos"
          }
        },
        "mirties": {
          "fields": {
            "data": {
              "type": "date",
              "dateFormat": "YYYY-MM-DD",
            }
          },
          "dependencies": {
            "type": "mirties"
          }
        },
      }
    },
    "view": "bootstrap-edit-horizontal"
  });
}


$(function () {
  load_image_tools();
  load_data_form();
});
