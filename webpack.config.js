module.exports = {
  entry: './ui/index.js',
  output: {
    path: __dirname + '/assets',
    filename: 'metrikai.bundle.js',
    publicPath: '/static/',

  },
  module: {
    rules: [
      {test: /\.css$/, use: [
        {loader: "style-loader"},
        {loader: "css-loader"},
      ]},
      {test: /\.(handlebars|hbs)$/, use: [
        {loader: "handlebars-loader"},
      ]},
      {test: /\.(eot|svg|ttf|woff|woff2)$/, use: [
        {loader: 'file-loader'}
      ]}
    ]
  },
  node: {
    // ERROR in node_modules/handlebars/lib/index.js
    // Module not found: Error: Can't resolve 'fs' in 'node_modules/handlebars/lib'
    fs: 'empty'
  },
  resolve: {
    alias: {
      // WARNING in node_modules/handlebars/lib/index.js 24:2-20
      // require.extensions is not supported by webpack. Use a loader instead.
      // https://github.com/wycats/handlebars.js/issues/953#issuecomment-239874313
      'handlebars' : 'handlebars/dist/handlebars.js'
    }
  },
  devtool: 'eval',
};
